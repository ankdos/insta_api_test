from flask import jsonify
from insta_api.helper import Insta_api
from flask import request
from flask import Flask, render_template , redirect , url_for , g
import sqlite3 as sql



app = Flask(__name__)


@app.route('/' , methods = ['POST','GET'])
def connect_to_insta() :
    redirect_uri = 'https://ancient-hamlet-72617.herokuapp.com/get_access_token'
    client_id = '10212297ff324c4d85faecb1c092ede2'
    URL = "https://api.instagram.com/oauth/authorize/?client_id=%s&redirect_uri=%s&response_type=code&scope=basic" % (client_id,redirect_uri)
    return render_template('connect.html' , url = URL)


@app.route('/get_access_token', methods = ['POST','GET'])
def get_user_data() :
    code = request.args.get('code')
    obj = Insta_api()
    obj.set_client_code(code) 
    return obj.endpoint_get_recent_post_data_by_secured_request()
    # return obj.endpoint_get_general_data_by_secured_request()
     

if __name__ == '__main__' :
    app.run()
